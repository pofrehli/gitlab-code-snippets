Dans le projet, il faut ajouter la gestion
des utilisateurs.

Implémentation de :
- Création des utilisateurs
- Recherche des utilisateurs
- Mise à jour des utilisateurs
- Suppression des utilisateurs

Tâches à faire :
- [x] Création des utilisateurs
- [ ] Recherche des utilisateurs
- [ ] Mise à jour des utilisateurs
- [ ] Suppression des utilisateurs
- [ ] Suppression de la base de données